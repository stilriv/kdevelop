# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2008, 2009.
# Stelios <sstavra@gmail.com>, 2011, 2019.
# Dimitris Kardarakos <dimkard@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kdevcmakebuilder\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2019-08-27 10:14+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: cmakebuilder.cpp:105
#, kde-format
msgid "Could not find the make builder. Check your installation"
msgstr "Αδυναμία εύρεσης του κατασκευαστή make. Ελέγξτε την εγκατάστασή σας"

#: cmakebuilder.cpp:129 cmakebuilder.cpp:155 cmakebuilder.cpp:182
#, kde-format
msgid "Could not find a builder for %1"
msgstr "Αδυναμία εύρεσης κατασκευαστή για το %1"

#: cmakebuilder.cpp:195
#, fuzzy, kde-format
#| msgid "No Build Directory configured, cannot install"
msgid "No build directory configured, cannot install"
msgstr "Δεν έχει διαμορφωθεί κατάλογος κατασκευής, αδυναμία εγκατάστασης"

#: cmakebuilder.cpp:205
#, fuzzy, kde-format
#| msgid "No Build Directory configured, cannot configure"
msgid "No build directory configured, cannot configure"
msgstr "Δεν έχει διαμορφωθεί κατάλογος κατασκευής, αδυναμία διαμόρφωσης"

#: cmakebuilderpreferences.cpp:40
#, fuzzy, kde-format
#| msgid "CMake"
msgctxt "@title:tab"
msgid "CMake"
msgstr "CMake"

#: cmakebuilderpreferences.cpp:45
#, fuzzy, kde-format
#| msgid "Configure Global CMake Settings"
msgctxt "@title:tab"
msgid "Configure Global CMake Settings"
msgstr "Διαμόρφωση καθολικών ρυθμίσεων CMake"

#. i18n: ectx: property (text), widget (QLabel, label1)
#: cmakebuilderpreferences.ui:20
#, fuzzy, kde-format
#| msgid "Default CMake executable:"
msgctxt "@label:chooser"
msgid "Default CMake executable:"
msgstr "Προκαθορισμένο CMake εκτελέσιμο:"

#. i18n: ectx: property (text), widget (QLabel, label2)
#: cmakebuilderpreferences.ui:37
#, fuzzy, kde-format
#| msgid "Default generator:"
msgctxt "@label:listbox"
msgid "Default generator:"
msgstr "Προκαθορισμένη γεννήτρια:"

#: cmakejob.cpp:38 prunejob.cpp:25
#, kde-format
msgid "CMake"
msgstr "CMake"

#: cmakejob.cpp:56
#, kde-format
msgid "Internal error: no project specified to configure."
msgstr ""

#: cmakejob.cpp:63
#, fuzzy, kde-format
#| msgid "Wrong build directory, cannot clear the build directory"
msgid "Failed to create build directory %1."
msgstr ""
"Λανθασμένος κατάλογος κατασκευής, αδυναμία καθαρισμού του καταλόγου "
"κατασκευής"

#: cmakejob.cpp:139
#, kde-format
msgid "CMake: %1"
msgstr "CMake: %1"

#: prunejob.cpp:39
#, fuzzy, kde-format
#| msgid "No Build Directory configured, cannot clear the build directory"
msgid "No build directory configured, cannot clear the build directory"
msgstr ""
"Δεν έχει διαμορφωθεί κατάλογος κατασκευής, αδυναμία καθαρισμού του καταλόγου "
"κατασκευής"

#: prunejob.cpp:45
#, kde-format
msgid "Wrong build directory, cannot clear the build directory"
msgstr ""
"Λανθασμένος κατάλογος κατασκευής, αδυναμία καθαρισμού του καταλόγου "
"κατασκευής"

#: prunejob.cpp:57
#, kde-format
msgid "%1> rm -rf %2"
msgstr "%1> rm -rf %2"

#: prunejob.cpp:72
#, kde-format
msgid "** Prune successful **"
msgstr "** Prune successful **"

#: prunejob.cpp:74
#, kde-format
msgid "** Prune failed: %1 **"
msgstr "** Prune failed: %1 **"

#, fuzzy
#~| msgid "Aborting build"
#~ msgid "Aborting"
#~ msgstr "Εγκατάλειψη κατασκευής"

#~ msgid "Aborting configure"
#~ msgstr "Εγκατάλειψη ρύθμισης"

#~ msgid "No Build Directory configured, cannot build"
#~ msgstr "Δεν έχει ρυθμιστεί κατάλογος κατασκευής, αδυναμία κατασκευής"

#~ msgid "No Build Directory configured, cannot clean"
#~ msgstr "Δεν έχει ρυθμιστεί κατάλογος κατασκευής, αδυναμία καθαρισμού"

#~ msgid "Aborting clean"
#~ msgstr "Εγκατάλειψη καθαρισμού"

#~ msgid "Aborting install"
#~ msgstr "Εγκατάλειψη εγκατάστασης"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stelios"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sstavra@gmail.com"

#~ msgid "CMake Builder"
#~ msgstr "Κατασκευαστής CMake"

#~ msgid "Support for building CMake projects"
#~ msgstr "Υποστήριξη και κατασκευή έργων CMake"

#~ msgid "No clearing of builddir possible"
#~ msgstr "Αδυναμία καθαρισμού του builddir"

#~ msgid "Job failed"
#~ msgstr "Αποτυχία εργασίας"

#~ msgid "CMake Binary:"
#~ msgstr "Εκτελέσιμο CMake:"

#~ msgid "Aborting builddir clearing"
#~ msgstr "Εγκατάλειψη καθαρισμού builddir"

#~ msgid "'%1' + '%2'"
#~ msgstr "'%1' + '%2'"
